import Constants from '../constants/Constants';
import UrlConfigConstants from '../constants/UrlConfigConstants';
import { PageContext,SessionContext, StoreContext, LocalContext } from 'rainbow-desktop-cache';

module.exports = {

    getSchemaKey(modelName, objectCode, productId) {
        return `${Constants.DD_SCHEMA_DATA_KEY}${Constants.COMM_CONNECTOR}${modelName}${Constants.COMM_CONNECTOR}${objectCode}${Constants.COMM_CONNECTOR}${productId}`;
    },

    getModeNameAndObjectCode(type) {
        const temp = type.split("-");
        return { "ModelName": temp[0], "ObjectCode": temp[1] };
    },
    loadModelObjectSchema(modelName, objectCode, productId, contextType) {       
        const key = this.getSchemaKey(modelName, objectCode, productId);  
            return new Promise((resolve, reject) => {
                StoreContext.get(key).then((productSchemaData) => {
                    if (productSchemaData) {
                        let localSchema = productSchemaData.originData
                        const SchemaList = localSchema['Objects'];
                        const RootObjectId = localSchema['RootObjectId'];
                        var buildSchema = this.findItemByObjectId(SchemaList, RootObjectId);
                        //去除地址引用关系
                        var buildSchemaStr = JSON.stringify(buildSchema);
                        var buildSchemaObj = JSON.parse(buildSchemaStr);

                        // this.buildSchema(SchemaList,RootObjectId);
                        PageContext.put(key, productSchemaData.originData);
                        PageContext.put(RootObjectId, buildSchemaObj);

                        resolve(productSchemaData.schemaData)
                    } else {
                        let uiProductSchemaData = PageContext.get(key);
                        if (uiProductSchemaData) {
                            resolve(this.convertSchema(uiProductSchemaData, key))
                        } else {
                            let data = {
                                modelName: modelName,
                                objectCode: objectCode,
                                contextType: contextType || Constants.PRODUCT_CONTEXT_TYPE,
                                referenceId: productId
                            };
                            const configList = JSON.parse(sessionStorage.getItem("project_config"));

                            $.ajax({
                                beforeSend: function(request) {
                                    const setionToken = SessionContext.get("Authorization");
                                    request.setRequestHeader("Authorization", 'Bearer ' + setionToken.substr(13).split("&")[0]);
                                    if(configList && configList.UI_CAS){
                                        request.setRequestHeader('x-ebao-auth-protocal',configList.UI_CAS);
                                    }
                                    if(configList && configList.UI_CAS_SERVICE_URL){
                                        request.setRequestHeader('x-ebao-cas-service-url',configList.UI_CAS_SERVICE_URL);
                                    }
                                    const SystemId = sessionStorage.getItem('x-ebao-system-id');
                                    if (SystemId) {
                                        request.setRequestHeader('x-ebao-system-id', SystemId);
                                    }
                                },
                                method: 'GET',
                                url: UrlConfigConstants.DD.product_dd_schema,
                                async: false,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                xhrFields: {withCredentials: true},
                                crossDomain: true,
                                data:data,
                                success: (response) => {
                                    PageContext.put(key, response);
                                    resolve(this.convertSchema(response, key))
                                }
                            });
                        }
                    }
                })
            });
            
        
    },

    convertSchema(localSchema, key){
       
        if(localSchema){
            const cacheSchema = PageContext.get(localSchema['RootObjectId']);
            if(cacheSchema){
                return cacheSchema;
            }else{
                // console.time('convertSchema')
                const SchemaList = localSchema['Objects'];
                const RootObjectId = localSchema['RootObjectId'];
                this.buildSchema(SchemaList,RootObjectId);
                const buildSchema =  this.findItemByObjectId(SchemaList,RootObjectId);
                //去除地址引用关系
                const buildSchemaStr = JSON.stringify(buildSchema);
                const buildSchemaObj = JSON.parse(buildSchemaStr);
                PageContext.put(RootObjectId,buildSchemaObj);

                let bigData = {originData: localSchema, schemaData: buildSchemaObj}
                if (key) {
                    StoreContext.put(key, bigData).then((obj) => {

                    })
                }
                
                // console.timeEnd('convertSchema')
                return buildSchemaObj;
            }
            
        }else{
            return null;
        }
    },
    
    getSchema(modelName, objectCode, productId){
        const key = this.getSchemaKey(modelName, objectCode, productId);
        const localSchema =  PageContext.get(key);
        return this.convertSchema(localSchema, key);
    },

    buildSchema(SchemaList,ObjectId){
        const schema = this.findItemByObjectId(SchemaList,ObjectId);
        if(!_.isEmpty(schema)){
            this.buildChild(schema,SchemaList);
            if(schema['SuperObjectId']){
                const SuperSchema = this.buildSchema(SchemaList,schema['SuperObjectId']);
                if(_.isEmpty(schema["ChildElements"])){
                    schema["ChildElements"]={}
                }
                _.each(_.keys(SuperSchema['ChildElements']),(key)=>{

                    if(_.isEmpty(schema['ChildElements'][key])){

                        schema['ChildElements'][key] = SuperSchema['ChildElements'][key];

                    }else{

                        if(schema['ChildElements'][key]['SuperObjectId']==SuperSchema['ChildElements'][key]['@pk']){
                            const superObject = SuperSchema['ChildElements'][key];
                            if(superObject&&superObject[0]&&superObject[0]['ContextId']>0){
                                schema['ChildElements'][key] = superObject;
                            }
                        }

                    }

                });
            }

        }
        return schema;
    },

    // removeCommonContextObject(schema){
    //     const childElements  = schema['ChildElements'];
    //     _.each(_.keys(childElements),(key)=>{
    //         const child = childElements[key];
    //         const ObjectName = child['ObjectName'];
    //         const ObjectCode = child['ObjectCode'];
    //         let deleteFlag = false;
    //         let commonKey = null;
    //         _.each(_.keys(childElements),(tempKey)=>{
    //             const tmpChild = childElements[tempKey];
    //             const tmpObjectName = tmpChild['ObjectName'];
    //             const tmpObjectCode = tmpChild['ObjectCode'];
    //             if(ObjectName===tmpObjectName&&ObjectCode===tmpObjectCode&&tmpChild['ContextId']>0){
    //                 deleteFlag = true;
    //             }
    //             if(tmpChild['ContextId']== -1){
    //                 commonKey = tempKey;
    //             }
    //         });
    //         if(deleteFlag&&commonKey){
    //             delete childElements[commonKey];
    //         }
    //     });
    // },

    buildChild(schema,SchemaList){
        const childs = schema["ChildElements"] || {};
        _.each(schema['Relations'],(Relation)=>{
            let child = this.findItemByObjectId(SchemaList,Relation['ObjectId']);
            if(!_.isEmpty(child)){
                if(child["RelationName"]){
                        child = _.cloneDeep(child);
                }
                child["RelationName"] = Relation.RelationName;
                const Lisykey = Relation.RelationName;
                // const Lisykey = child['ModelName']+'List';
                if(_.isEmpty(childs[Lisykey])){
                    childs[Lisykey] = [child];
                }else{
                    childs[Lisykey].push(child);
                }
                
                this.buildSchema(SchemaList,Relation['ObjectId']);
            }
            delete schema['Relations'];
        });
        if(!_.isEmpty(childs)){
            schema["ChildElements"]=_.cloneDeep(childs);
        }
    },


    findItemByObjectId(SchemaList,RootObjectId){
        const schema =  _.find(SchemaList,(item)=>{
            return item[Constants.COMM_PK]== RootObjectId;
        });
        if(_.isEmpty(schema)){
            return null;
        }else{
           schema['RelationType']= -2;
           return  schema;
        }
    },
    saveAndDeleteFields(object) {
        if (object["Fields"]) {
            //PageContext.put(object[Constants.COMM_TYPE], object["Fields"]);
            delete object["Fields"];
        }
        const childs = object["ChildElements"];
        if (childs) {
            _.each(_.keys(childs), (key) => {
                if (_.isArray(childs[key])) {
                    _.each(childs[key], (child) => {
                        this.saveAndDeleteFields(child);
                    });
                } else {
                    this.saveAndDeleteFields(childs[key]);
                }
            });
        }
    },

    getFields(object) {
        return PageContext.get(object[Constants.COMM_TYPE]);
    },
    getSchemaByModelName(param, schema) {
        const returnObject = [];
        if (schema) {
            this._getSchemaByModelName(param,schema,returnObject,schema);
            return returnObject
        } else {
            return null;
        }
    },
    _getSchemaByModelName(param, schema, returnObject, parentSchema) {
        if (schema[Constants.DD_MODEL_NAME] == param[Constants.DD_MODEL_NAME]) {
            if(param[Constants.PARENT_MODEL_NAME]&&parentSchema){
                if(param[Constants.PARENT_MODEL_NAME] == parentSchema[Constants.DD_MODEL_NAME]){
                    returnObject.push(schema);
                }
            }else{
                returnObject.push(schema);
            }
        }
        const childElements = schema[Constants.CHILD_ELEMENTS];
        if (childElements) {
            _.each(_.keys(childElements), (child) => {
                _.each(childElements[child], (childElement) => {
                    this._getSchemaByModelName(param,childElement,returnObject,schema);
                });
            });
        }
    },
    getScopeSchema(param,schema){
        if(param[Constants.SCOPE_MODEL_NAME]&&param[Constants.SCOPE_OBJECT_CODE]){
               const _param = {
                       'ModelName': param[Constants.SCOPE_MODEL_NAME],
                       'ObjectCode': param[Constants.SCOPE_OBJECT_CODE]
               }
               const returnSchema = this.lookupModelObjectSchema(_param, schema);
               if(returnSchema){
                    return returnSchema["CURRENT_SCHEMA"];
               }else{
                    return null;
               }
        }else{
            return null;
        }
    },
    lookupModelObjectSchema(param, schema) {
        const productId = param[Constants.PRODUCT_ID];
        const scopeSchema = this.getScopeSchema(param,schema);
        if(scopeSchema){
            schema = scopeSchema;
        }
        const returnObject = {};
        if (schema) {
            this._lookupModelObjectSchema(null, schema, param, returnObject);
            if (param[Constants.PARENT_MODEL_NAME] && param[Constants.PARENT_OBJECT_CODE]) {
                const parent_param = { "ModelName": param[Constants.PARENT_MODEL_NAME], "ObjectCode": param[Constants.PARENT_OBJECT_CODE], "ProductId": productId };
                const returnSchema = this.lookupModelObjectSchema(parent_param, schema);
                returnObject["PARENT_SCHEMA"] = returnSchema[Constants.CURRENT_SCHEMA];
            }
            return returnObject
        } else {
            return null;
        }
    },

    _lookupModelObjectSchema(parentSchema, schema, param, returnObject) {
        if (schema[Constants.DD_MODEL_NAME] == param[Constants.DD_MODEL_NAME] && schema[Constants.DD_OBJECT_CODE] == param[Constants.DD_OBJECT_CODE]) {
            returnObject["CURRENT_SCHEMA"] = schema;
            returnObject["PARENT_SCHEMA"] = parentSchema;
        }
        const childElements = schema[Constants.CHILD_ELEMENTS];
        if (childElements) {
            _.each(_.keys(childElements), (child) => {
                _.each(childElements[child], (childElement) => {
                    this._lookupModelObjectSchema(schema, childElement, param, returnObject);
                });
            });
        }
    }
}

