let UI_API_GATEWAY_PROXY = sessionStorage.getItem('project_config')?JSON.parse(sessionStorage.getItem('project_config')).UI_API_GATEWAY_PROXY:undefined;

module.exports = {
	UI_API_GATEWAY_PROXY: UI_API_GATEWAY_PROXY,
	DD: {
		product_dd_schema: UI_API_GATEWAY_PROXY + "dd/public/dictionary/v1/generateFullUiResourceSchemaWithoutFieldNew",
		product_dd_codetable: UI_API_GATEWAY_PROXY + "ui/common/field/codetable/byContextTypeAndReferenceId",
		product_dd_id_codetable: UI_API_GATEWAY_PROXY + "ui/common/codetable/byContextTypeAndReferenceId",
		product_common_codetable: UI_API_GATEWAY_PROXY + "ui/common/codetable/byContextTypeAndReferenceId",
		product_common_id_codetable: UI_API_GATEWAY_PROXY + "ui/common/codetable/byContextTypeAndReferenceId",
		product_dd_big_codetable: UI_API_GATEWAY_PROXY + "ui/common/codetable/version/single"
	},
    PRODUCT_API:{
        GET_PRODUCT_BY_CODE_VERSION:UI_API_GATEWAY_PROXY + "product/prd/v1/getProductIdByCodeAndVersion",
        GET_PRODUCT_BY_EFFORTDATE:UI_API_GATEWAY_PROXY + "product/prd/v1/getEffectiveProductByStartDate",
        GET_PRODUCT_BY_PRODUCT_ID:UI_API_GATEWAY_PROXY +'product/prd/v1/product?productId=',
        GET_PRODUCT_MASTER_BY_PRODUCT_CODE:UI_API_GATEWAY_PROXY +'product/prd/v1/productMaster/productCode?productCode=',
        LOAD_PRODUCT_BY_ID:UI_API_GATEWAY_PROXY +'product/element/runtime/v1/getElementTreeByProductId',
        LOAD_PRODUCT_ELEMENT_BY_ID:UI_API_GATEWAY_PROXY +'product/element/runtime/v1/getProductElement',
        GET_PLAN_BY_PLAN_CODE:UI_API_GATEWAY_PROXY +'plan/planResource/v1/loadByPlanCode?planCode=',
        GET_FILTER_FORM:UI_API_GATEWAY_PROXY +'product/element/runtime/v1/filterChildElement',
        GET_RELATION_ELEMENT:UI_API_GATEWAY_PROXY +'product/element/runtime/v1/getRecursiveRelationMap',
        GET_PRODUCT_ALL_RELATION:UI_API_GATEWAY_PROXY +'product/element/runtime/v1/getProductElementRelationList?productId=',
    },
    POLICY_API:{
        CREATE_POLICY:UI_API_GATEWAY_PROXY + "public/orchestration/dispatch/newbiz_Quoting",
        FILL_POLICY:UI_API_GATEWAY_PROXY + "pa/pa/policy/v1/fill/policy/product"
    },
    ENDORSEMENT_API:{
        CREATE_ENDORSEMENT:UI_API_GATEWAY_PROXY + "public/orchestration/dispatch/AP00_endo_create",
		SAVE_ENDORSEMENT:UI_API_GATEWAY_PROXY + "public/orchestration/dispatch/AP00_endo_update"
    }
};
